#!/bin/sh
gpart add -a 4k -s 800K -t efi ada0
gpart bootcode -p /boot/boot1.efifat -i 1 ada0

gpart add -a 1m -t freebsd-zfs -l disk0 ada0
mount -t tmpfs tmpfs /mnt

zpool create -f -o altroot=/mnt zroot ada0p5
zfs set compress=on                                            zroot
zfs create -o mountpoint=none                                  zroot/ROOT
zfs create -o mountpoint=/ -o canmount=noauto                  zroot/ROOT/default
mount -t zfs zroot/ROOT/default /mnt
#mkdir -p /mnt/boot
#mount /dev/ada0p4 /mnt/boot
zfs create -o mountpoint=/tmp  -o exec=on      -o setuid=off   zroot/tmp
zfs create -o canmount=off -o mountpoint=/usr                  zroot/usr
zfs create                                                     zroot/usr/home
zfs create                     -o exec=off     -o setuid=off   zroot/usr/src
zfs create                                                     zroot/usr/obj
zfs create -o mountpoint=/usr/ports            -o setuid=off   zroot/usr/ports
zfs create                     -o exec=off     -o setuid=off   zroot/usr/ports/distfiles
zfs create                     -o exec=off     -o setuid=off   zroot/usr/ports/packages
zfs create -o canmount=off -o mountpoint=/var                  zroot/var
zfs create                     -o exec=off     -o setuid=off   zroot/var/audit
zfs create                     -o exec=off     -o setuid=off   zroot/var/crash
zfs create                     -o exec=off     -o setuid=off   zroot/var/log
zfs create -o atime=on         -o exec=off     -o setuid=off   zroot/var/mail
zfs create                     -o exec=on      -o setuid=off   zroot/var/tmp
ln -s /usr/home /mnt/home
chmod 1777 /mnt/var/tmp
chmod 1777 /mnt/tmp
zpool set bootfs=zroot/ROOT/default zroot
cat << EOF > /zroot/etc/fstab
# Device                       Mountpoint              FStype  Options         Dump    Pass#
/dev/gpt/linux%20swap                 none                    swap    sw              0       0
EOF