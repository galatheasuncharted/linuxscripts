#!/bin/bash
pacman -S reflector
reflector --verbose --country 'Germany' -l 200 -p https --sort rate --save /etc/pacman.d/mirrorlist

mkfs.vfat -F 32 -n BOOT /dev/sda1
mkfs.ext4 -L ARCHROOT /dev/sda2
mkswap -L SWAP /dev/sda3

mount /dev/sda2 /mnt
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot
swapon /dev/sda3

pacstrap /mnt base base-devel linux linux-firmware nano bash-completion

genfstab -Lp /mnt > /mnt/etc/fstab

cp chroot.sh /mnt/root
chmod +x /mnt/root/chroot.sh
