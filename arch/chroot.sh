#!/bin/bash

####### USER VARIABLES
# Bootloader Device?
# default is sda
DEVICE=/dev/sda

# Timezone
# default is Europe/Berlin
TIMEZONE=Europe/Berlin

# SSD Option
# Installed on a SSD?
# true/false
# default true
SSD=false

# Your Username
# Change it.
MYUSER=gala

# Your default group
# it is recommend to use the group "users"!
MYUSER_GROUP=users

# Optional Groups
# For the Administrator Account
# it is recommend to invite him into
# wheel audio and vido
# add some more if you need and like
MYUSER_GROUPS="wheel,audio,video"

# Your Shell you want to use
# default is BASH
MYBASH=/bin/bash

# Your System Language
# default is de_DE.UTF-5
# You can change it to other 
# if you prefere Languages 
MYLANG=LANG=de_DE.UTF-8

# Your systemwide Keymap
MYKEYMAP=KEYMAP=de-latin1-nodeadkeys
MYXKEYMAP=de

# Your Hostname
# You can change it to everything you want
HOSTNAME=archscript

# Your Graphic Adapter
# default virtualbox-guest-utils
# GRAPHIC="nvidia nvidia-utils"
# GRAPHIC=xf86-video-ati
GRAPHIC=virtualbox-guest-utils


# IMPORTANT!
# This is the Option for make
# how many threads you want to use
# to compile Software.
# Please change it to your threadnumber + 1
# eg you got a 2 Thread Processor 
# you have to change it to 3
# if you have a 12 thread Processor change to 13
MAKETHREADS=7


###### DO NOT CHANGE ANYTHING 
###### UNLESS YOUR ARE REALY 
###### SURE WHAT YOU ARE DOING!!!!
HOSTNAME_FILE=/etc/hostname
LOCALE_CONF=/etc/locale.conf
LOCALE_GEN=/etc/locale.gen
VCONSOLE=/etc/vconsole.conf
LOCALTIME=/etc/localtime
SUDOERS=/etc/sudoers

pacman -S reflector
reflector --verbose --country 'Germany' -l 200 -p https --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syyu

mkinitcpio -p linux

pacman -S grub dosfstools efibootmgr mtools
mkdir -p /boot/grub
grub-mkconfig -o /boot/grub/grub.cfg
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub

echo $HOSTNAME > $HOSTNAME_FILE
echo $MYLANG > $LOCALE_CONF
sed -i 's/#de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g' $LOCALE_GEN
locale-gen
echo $MYKEYMAP > $VCONSOLE
ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
echo "Bitte Passwort für Root vergeben!"
passwd
useradd -m -g $MYUSER_GROUP -G $MYUSER_GROUPS -s /bin/bash $MYUSER
echo "Bitte Passwort für $MYUSER vergeben"
passwd $MYUSER
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' $SUDOERS

if [ $SSD ]
then
	systemctl enable fstrim.timer
fi

echo "Das System ist soweit Konfiguriert. Fangen wir mit der installation des Desktops an"
sleep 3
pacman -S acpid avahi cups cronie xorg-server xorg-xinit ${GRAPHIC} \
            xfce4 xfce4-goodies lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings \
            networkmanager network-manager-applet nm-connection-editor alsa-tools \
            alsa-utils pulseaudio-alsa pavucontrol noto-fonts arc-icon-theme arc-gtk-theme

echo "Installation GrundSystem ist fertig?"
sleep 3
systemctl enable acpid avahi-daemon org.cups.cupsd.service
systemctl enable cronie
systemctl enable systemd-timesyncd.service
hwclock -w
systemctl enable lightdm.service NetworkManager

echo "Internet Tools"
pacman -S firefox firefox-i18n-de chromium filezilla hexchat teamspeak3 \
		  tigervnc nmap
		  
echo "Weitere System Spezifische Tools"
pacman -S gparted grsync gsmartcontrol htop xarchiver


echo "Section \"InputClass\"" > /etc/X11/xorg.conf.d/20-keyboard.conf
echo "      Identifier \"keyboard\"" >> /etc/X11/xorg.conf.d/20-keyboard.conf
echo "      MatchIsKeyboard \"yes\"" >> /etc/X11/xorg.conf.d/20-keyboard.conf
echo "      Option \"XkbLayout\" \"de\"" >> /etc/X11/xorg.conf.d/20-keyboard.conf
echo "      Option \"XkbModel\" \"pc105\"" >> /etc/X11/xorg.conf.d/20-keyboard.conf
echo "      Option \"XkbVariant\" \"nodeadkeys\"" >> /etc/X11/xorg.conf.d/20-keyboard.conf
echo "EndSection" >> /etc/X11/xorg.conf.d/20-keyboard.conf


echo "Erledigt!"
echo "Sie können jetzt mit exit die Chroot Umgebung verlassen,"
echo "Anschließend müssen Sie mit dem Befehl umount -R /mnt "
echo "alle Festplatten Aushängen und mit swappoff ${DEVICE}3"
echo "den Swapspeicher deaktivieren."
echo "Benutzen Sie anschließend reboot um das System neu zustarten."
echo "Vergessen Sie bitte nicht das Installations Medium vor dem Neustart zu entfernen!"

#cd /home/$MYUSER
#mkdir .AUR
#cd .AUR
#git clone https://aur.archlinux.org/aurman.git
#cd ..
#chown -R $MYUSER:users .AUR

echo "Software über aur installieren:"
echo "aurman discord lcms libc++abi libc++ minecraft-launcher pamac-aur purevpn-networkmanager rambox-bin ttf-ms-fonts worldpainter"
echo "Viel spaß mit deinem Arch Linux"

exit 0
